# README #

Análisis Descriptivo de datos: Casos positivos por COVID-19 en el departamento de Lambayeque

### What is this repository for? ###

* Los gráficos presentados en Tableau pretenden ilustrar la situación actual del departamento de Lambayaque con respescto a los infectados por el COVID-19 según el Ministerio de Salud.

### Datasets usados ###

* Casos positivos por COVID-19 - [Ministerio de Salud - MINSA]: https://www.datosabiertos.gob.pe/dataset/casos-positivos-por-covid-19-ministerio-de-salud-minsa

### Limpieza de datos ###

* A través de Tableau apliqué los filtros correspondientes para sectorizar la información del dataset.
* Sólo fue necesario tratar algunos datos NULL para el caso de edades y sexo.

### Ilustraciones elaboradas ###

* Comparación geográfica de infectados por el COVID-19
* Cantidad de infectados por distrito
* Cantidad de infectados según sexo
* Cantidad de infectados según edad
* Calendario de infectados según fecha de resultado

### Video ###
* https://drive.google.com/file/d/1gT-S85qYSpwW3AGJDV2FuniuuYCjptuU/view?usp=sharing